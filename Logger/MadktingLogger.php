<?php
/**
 * Madkting Software (http://www.madkting.com)
 *
 *                                      ..-+::moossso`:.
 *                                    -``         ``ynnh+.
 *                                 .d                 -mmn.
 *     .od/hs..sd/hm.   .:mdhn:.   yo                 `hmn. on     mo omosnomsso oo  .:ndhm:.   .:odhs:.
 *    :hs.h.shhy.d.mh: :do.hd.oh:  /h                `+nm+  dm   ys`  ````mo```` hn :ds.hd.yo: :oh.hd.dh:
 *    ys`   `od`   `h+ sh`    `do  .d`              `snm/`  +s hd`        hd     yy yo`    `sd oh`    ```
 *    hh     sh     +m hs      yy   y-            `+mno`    dkdm          +d     o+ no      ss ys    dosd
 *    y+     ss     oh hdsomsmnmy   ++          .smh/`      om ss.        dh     mn yo      oh sm      hy
 *    sh     ho     ys hs``````yy   .s       .+hh+`         ys   hs.      os     yh os      d+ od+.  ./m/
 *    od     od     od od      od   +y    .+so:`            od     od     od     od od      od  `syssys`
 *                                 .ys .::-`
 *                                o.+`
 *
 * @category Module
 * @package Madkting\Connect
 * @author Carlos Guillermo Jiménez Salcedo <guillermo@madkting.com>
 * @link https://bitbucket.org/madkting/magento2
 * @copyright Copyright (c) 2017 Madkting Software.
 * @license See LICENSE.txt for license details.
 */

namespace Madkting\Connect\Logger;

use Madkting\Connect\Model\Config;
use Madkting\Exception\MadktingException;
use Magento\Framework\App\Area;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Store\Model\StoreManagerInterface;
use Monolog\Logger;

/**
 * Class MadktingLogger
 * @package Madkting\Connect\Log
 */
class MadktingLogger extends Logger
{
    /**
     * Default title for emails
     */
    const DEFAULT_TITLE = 'Madkting Error';

    /**
     * Template ID
     */
    const TEMPLATE_ID = 'madkting_error_email';

    /**
     * @var array
     */
    protected $senderInfo = ['name' => 'Madkting Support Magento 2', 'email' => 'magento-error@madkting.com'];

    /**
     * @var TransportBuilder
     */
    protected $transportBuilder;

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * MadktingLogger constructor
     *
     * @param TransportBuilder $transportBuilder
     * @param Config $config
     * @param StoreManagerInterface $storeManager
     * @param string $name
     * @param array $handlers
     * @param array $processors
     */
    public function __construct(
        TransportBuilder $transportBuilder,
        Config $config,
        StoreManagerInterface $storeManager,
        $name,
        array $handlers = array(),
        array $processors = array()
    ) {
        parent::__construct($name, $handlers, $processors);
        $this->transportBuilder = $transportBuilder;
        $this->config = $config;
        $this->storeManager = $storeManager;
    }

    /**
     * @param int $level
     * @param string $message
     * @param array $context
     * @param bool $sendMail
     * @param bool $logCase
     * @return int|bool
     */
    public function log($level, $message, array $context = array(), $sendMail = false, $logCase = false)
    {
        /* Process Madkting data */
        $logData = $this->getLogData($message, $context, $logCase);

        /* Add record to logger */
        $log = parent::log($level, $logData['message'], $logData['context']);

        /* If is enabled email */
        if ($sendMail) $this->sendMail($message, $logData);

        /* If is requested case number exists */
        return $logCase && !empty($logData['case']) ? $logData['case'] : $log;
    }

    /**
     * @param string $message
     * @param array $context
     * @param bool $sendMail
     * @param bool $logCase
     * @return int|bool
     */
    public function debug($message, array $context = array(), $sendMail = true, $logCase = true)
    {
        return $this->log(self::DEBUG, $message, $context, $sendMail, $logCase);
    }

    /**
     * @param string $message
     * @param array $context
     * @param bool $sendMail
     * @param bool $logCase
     * @return bool
     */
    public function info($message, array $context = array(), $sendMail = false, $logCase = false)
    {
        return $this->log(self::INFO, $message, $context, $sendMail, $logCase);
    }

    /**
     * @param string $message
     * @param array $context
     * @param bool $sendMail
     * @param bool $logCase
     * @return bool
     */
    public function notice($message, array $context = array(), $sendMail = false, $logCase = false)
    {
        return $this->log(self::NOTICE, $message, $context, $sendMail, $logCase);
    }

    /**
     * @param string $message
     * @param array $context
     * @param bool $sendMail
     * @param bool $logCase
     * @return bool
     */
    public function warn($message, array $context = array(), $sendMail = false, $logCase = true)
    {
        return $this->log(self::WARNING, $message, $context, $sendMail, $logCase);
    }

    /**
     * @param string $message
     * @param array $context
     * @param bool $sendMail
     * @param bool $logCase
     * @return bool
     */
    public function warning($message, array $context = array(), $sendMail = false, $logCase = true)
    {
        return $this->log(self::WARNING, $message, $context, $sendMail, $logCase);
    }

    /**
     * @param string $message
     * @param array $context
     * @param bool $sendMail
     * @param bool $logCase
     * @return bool
     */
    public function err($message, array $context = array(), $sendMail = false, $logCase = true)
    {
        return $this->log(self::ERROR, $message, $context, $sendMail, $logCase);
    }

    /**
     * @param string $message
     * @param array $context
     * @param bool $sendMail
     * @param bool $logCase
     * @return bool
     */
    public function error($message, array $context = array(), $sendMail = false, $logCase = true)
    {
        return $this->log(self::ERROR, $message, $context, $sendMail, $logCase);
    }

    /**
     * @param string $message
     * @param array $context
     * @param bool $sendMail
     * @param bool $logCase
     * @return bool
     */
    public function crit($message, array $context = array(), $sendMail = false, $logCase = true)
    {
        return $this->log(self::CRITICAL, $message, $context, $sendMail, $logCase);
    }

    /**
     * @param string $message
     * @param array $context
     * @param bool $sendMail
     * @param bool $logCase
     * @return bool
     */
    public function critical($message, array $context = array(), $sendMail = false, $logCase = true)
    {
        return $this->log(self::CRITICAL, $message, $context, $sendMail, $logCase);
    }

    /**
     * @param string $message
     * @param array $context
     * @param bool $sendMail
     * @param bool $logCase
     * @return bool
     */
    public function alert($message, array $context = array(), $sendMail = false, $logCase = true)
    {
        return $this->log(self::ALERT, $message, $context, $sendMail, $logCase);
    }

    /**
     * @param string $message
     * @param array $context
     * @param bool $sendMail
     * @param bool $logCase
     * @return bool
     */
    public function emerg($message, array $context = array(), $sendMail = false, $logCase = true)
    {
        return $this->log(self::EMERGENCY, $message, $context, $sendMail, $logCase);
    }

    /**
     * @param string $message
     * @param array $context
     * @param bool $sendMail
     * @param bool $logCase
     * @return bool
     */
    public function emergency($message, array $context = array(), $sendMail = false, $logCase = true)
    {
        return $this->log(self::EMERGENCY, $message, $context, $sendMail, $logCase);
    }

    /**
     * @param \Throwable|\Exception $exception
     * @param string $message
     * @param array $context
     * @param bool $sendMail
     * @param bool $logCase
     * @return bool
     */
    public function exception($exception, $message, array $context = array(), $sendMail = false, $logCase = true)
    {
        /* Add exception string to context */
        $context['exception'][] = get_class($exception) . ': ' . $exception->getMessage() . ' in ' . $exception->getFile() . ':' . $exception->getLine();
        $context['exceptionString'] = $exception->__toString();

        /* If is Madkting exception complete $context and turn on send mail */
        if ($exception instanceof MadktingException) {
            $sendMail = true;
            $context['exception']['response'] = $exception->getResponse();
            $context['exception']['request'] = $exception->getRequest();
            $context['exception']['connectionError'] = $exception->isConnectionError();
            $context['exception']['result'] = $exception->getResult();
        }

        return $this->log(self::ERROR, $message, $context, $sendMail, $logCase);
    }

    /**
     * @param string $message
     * @param array $context
     * @param bool $logCase
     * @return array
     */
    protected function getLogData($message, &$context, $logCase = false)
    {
        /* Set title */
        if (!empty($context['title'])) {
            $title = $context['title'];
            unset($context['title']);
        }
        $message = !empty($title) ? $title.': '.$message : $message;

        /* Get exception string */
        if (!empty($context['exceptionString'])) {
            $exceptionString = $context['exceptionString'];
            unset($context['exceptionString']);
        }

        /* Set log case */
        if ($logCase) {
            $caseNumber = date('ymd') . rand(10000,99999);
            $message = __('#%1 %2', $caseNumber, $message);
        } else $caseNumber = null;

        /* Context */
        if (!empty($context)) $message .= ' ' . json_encode($context);

        return [
            'title' => !empty($title) ? $title : self::DEFAULT_TITLE,
            'message' => $message,
            'context' => $context,
            'case' => $caseNumber,
            'exception' => !empty($exceptionString) ? $exceptionString : false
        ];
    }

    /**
     * @param string $message
     * @param array $logData
     */
    protected function sendMail($message, $logData)
    {
        try{
            /* Get store info */
            $storeId = $this->config->getSelectedStore();
            $storeName = $this->config->getValue('general/store_information/name', $storeId);
            $storeBaseUrl = $this->storeManager->getStore($storeId)->getBaseUrl();
            $subjectStore = !empty($storeName) ? $storeName . ' ' . $logData['title'] : $storeBaseUrl . ' ' . $logData['title'];

            /* Get email's vars */
            $emailVars = [
                'subject' => $subjectStore,
                'title' => $logData['title'],
                'store' => $storeName,
                'url' => $storeBaseUrl,
                'message' => $message,
                'context' => json_encode($logData['context'])
            ];

            /* Get receiver information */
            $receiverInfo = ['Madkting Support' => 'guillermo@madkting.com'];

            /* Send email */
            $this->transportBuilder->setTemplateIdentifier(self::TEMPLATE_ID)
                ->setTemplateOptions(['area' => Area::AREA_ADMINHTML, 'store' => $storeId])
                ->setTemplateVars($emailVars)
                ->setFrom($this->senderInfo)
                ->addTo($receiverInfo)
                ->getTransport()
                ->sendMessage();
        } catch (\Exception $e) {
            $this->info(__('Make sure SMTP is configured correctly, %1', $e->getMessage()));
        }
    }
}
